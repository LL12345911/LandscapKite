//
//  UIWindow+CurrentViewController.h
//  EngineeringCool
//
//  Created by Mars on 2023/9/4.
//  Copyright © 2023 Mars. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIWindow (CurrentViewController)

+ (UIViewController *)currentViewController;

@end

NS_ASSUME_NONNULL_END
