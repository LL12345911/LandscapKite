//
//  ATTabBarViewController.h
//  EngineeringCool
//
//  Created by Mars on 2022/3/11.
//  Copyright © 2022 Mars. All rights reserved.
//

#import "ATTabBarViewController.h"

@implementation ATTabBarViewController

//// 是否支持转屏
//- (BOOL)shouldAutorotate
//{
//    return [self.selectedViewController shouldAutorotate];
//}
//
//
//// 返回nav栈中的最后一个对象支持的旋转方向
//- (UIInterfaceOrientationMask)supportedInterfaceOrientations
//{
//    return [self.selectedViewController supportedInterfaceOrientations];
//}
//
//
//// 返回nav栈中最后一个对象,坚持旋转的方向
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
//{
//    return [self.selectedViewController preferredInterfaceOrientationForPresentation];
//}


// 是否支持自动转屏
- (BOOL)shouldAutorotate {
    UIViewController *vc = self.viewControllers[self.selectedIndex];
    if ([vc isKindOfClass:[UINavigationController class]]) {
        UINavigationController *nav = (UINavigationController *)vc;
        return [nav.topViewController shouldAutorotate];
    } else {
        return [vc shouldAutorotate];
    }
}
// 支持哪些屏幕方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    UIViewController *vc = self.viewControllers[self.selectedIndex];
    if ([vc isKindOfClass:[UINavigationController class]]) {
        UINavigationController *nav = (UINavigationController *)vc;
        return [nav.topViewController supportedInterfaceOrientations];
    } else {
        return [vc supportedInterfaceOrientations];
    }
}
// 默认的屏幕方向（当前ViewController必须是通过模态出来的UIViewController（模态带导航的无效）方式展现出来的，才会调用这个方法）
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    UIViewController *vc = self.viewControllers[self.selectedIndex];
    if ([vc isKindOfClass:[UINavigationController class]]) {
        UINavigationController *nav = (UINavigationController *)vc;
        return [nav.topViewController preferredInterfaceOrientationForPresentation];
    } else {
        return [vc preferredInterfaceOrientationForPresentation];
    }
}



@end
