//
//  RotationManager.m
//  EngineeringCool
//
//  Created by Mars on 2023/9/4.
//  Copyright © 2023 Mars. All rights reserved.
//

#import "RotationManager.h"
#import <UIKit/UIKit.h>

@interface RotationManager()

// 横屏状态
@property(nonatomic, copy) NSString *rotationStatus;

@end

@implementation RotationManager


//单例方法
+(instancetype)shareInstance{
    static RotationManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        if (!manager) {
            manager = [[RotationManager alloc] init];
        }
    });
    
    return manager;
}

- (instancetype)init {
    if (self = [super init]) {
        _isRotation = NO;
        // 初始化一个测试类，类里面声明一个属性：rotationStatus
        // 给属性nameStr设置监听,类型不同，回调值不同，这里监听的是新值（NSKeyValueObservingOptionNew），还可以监听其他变化，具体看枚举
        [self addObserver:self forKeyPath:@"rotationStatus" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:nil];
    }
    return self;
}

- (void)setIsRotation:(BOOL)isRotation {
    _isRotation = isRotation;
    if(_isRotation) {
        self.rotationStatus = @"Landscape";
    }else {
        self.rotationStatus = @"Portrait";
    }
}

/**
  KVO是根据iOS runtime实现的，当监听某个对象（_kvoTest）的某个属性时，KVO会创建这个对象的子类，并重写我们监听的属性（keyPath）的set方法.
 
 
 @discussion didChangeValueForKey:方法执行完之后，KVO会根据注册时option返回不同的字典
 @discussion KVO是建立在KVC基础上的，可以看到，如果不通过KVC改变属性，那么set方法就不会执行，那么KVO也就没办法监听属性的变化了。
 
 @discussion 当观察对象时，KVO机制动态创建一个新的名为：NSKVONotifying_对象名 的新类，该类继承自目标对象的本类，且 KVO 为 NSKVONotifying_对象名重写观察属性的set方法。在这个过程，被观察对象的isa指针从指向原来的对象，被KVO机制修改为指向系统新创建的子类NSKVONotifying_对象名类，来实现当前类属性值改变的监听，这也就是前面所说的“黑魔法”；我还试了一下，创建一个新的名为“NSKVONotifying_对象名”的类，发现系统运行到注册KVO的代码时，iOS10及以下会崩溃，iOS11下控制台打印警告：
 ```
 [general] KVO failed to allocate class pair for name NSKVONotifying_KVOTestModel,
 automatic key-value observing will not work for this class
 ```
 */
- (void)setRotationStatus:(NSString *)rotationStatus {
   [self willChangeValueForKey:@"rotationStatus"];
    _rotationStatus = rotationStatus;
    if (@available(iOS 16.0, *)) {
       // [[RotationManager presentController].navigationController setNeedsUpdateOfSupportedInterfaceOrientations];
        
        [[RotationManager presentController] setNeedsUpdateOfSupportedInterfaceOrientations];
    }else{
        [UIViewController attemptRotationToDeviceOrientation];
    }
   [self didChangeValueForKey:@"rotationStatus"];
}


// 监听的属性通过kvc发生变化时，执行下面方法
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
   // 这里我们只判断了keyPath，如果存在多个kvo，为防止重名，可判断object是否为我们监听的实例：_kvoTest
   if ([keyPath isEqualToString:@"rotationStatus"]) {
       // 我们直接打印新值
       NSString *newStr = [NSString stringWithFormat:@"%@", [change objectForKey:NSKeyValueChangeNewKey]];
       NSString *oldStr = [NSString stringWithFormat:@"%@", [change objectForKey:NSKeyValueChangeOldKey]];
#ifdef DEBUG
       NSLog(@"\n");
       NSLog(@"⭐⭐⭐change: new == %@⭐⭐⭐", newStr);
       NSLog(@"⭐⭐⭐change: old == %@⭐⭐⭐", oldStr);
#endif
       //if ([newStr isEqualToString:@"Portrait"]) {
       if (newStr && oldStr && [newStr isEqualToString:@"Portrait"] && [oldStr isEqualToString:@"Landscape"]) {
#ifdef DEBUG
           NSLog(@"⭐⭐⭐执行强制竖屏方法⭐⭐⭐");
           NSLog(@"\n");
#endif
           // 强制竖屏
           UIViewController *vc = [RotationManager presentController];
           [RotationManager portrait:vc];
           
       }else  if([newStr isEqualToString:@"Landscape"]){
           
          // UIInterfaceOrientationLandscapeRight
           UIViewController *vc = [RotationManager presentController];
           [RotationManager landscape:vc];
       }
       
   }
}

/** 竖屏 */
+ (void)portrait:(UIViewController *)vc {
    if (@available(iOS 16.0, *)) {
        [vc setNeedsUpdateOfSupportedInterfaceOrientations];
        [self orientationSwitch:UIInterfaceOrientationMaskPortrait];
    } else {
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    }
}

/** 横屏 */
+ (void)landscape:(UIViewController *)vc {
    if (@available(iOS 16.0, *)) {
        [vc setNeedsUpdateOfSupportedInterfaceOrientations];
        [self orientationSwitch:UIInterfaceOrientationMaskLandscapeRight];
    } else {
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeRight];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    }
}

/** 横竖屏切换 */
+ (void)orientationSwitch:(UIInterfaceOrientationMask)orientationMask {
    if (@available(iOS 16.0, *)) {
        NSArray *array = [[[UIApplication sharedApplication] connectedScenes] allObjects];
        UIWindowScene *scene = [array firstObject];
        //UIInterfaceOrientationMask orientationMask = scene.interfaceOrientation == UIInterfaceOrientationPortrait ? UIInterfaceOrientationMaskLandscapeLeft : UIInterfaceOrientationMaskPortrait;
        UIWindowSceneGeometryPreferencesIOS *geometryPreferencesIOS = [[UIWindowSceneGeometryPreferencesIOS alloc] initWithInterfaceOrientations:orientationMask];
        [scene requestGeometryUpdateWithPreferences:geometryPreferencesIOS errorHandler:^(NSError * _Nonnull error) {
            
        }];
    }
}


- (void)dealloc {
    //删除指定的key路径监听器
    [self removeObserver:self forKeyPath:@"rotationStatus"];
}

#pragma mark -
#pragma mark - 强制竖屏



#pragma mark -
#pragma mark - 查询需要的屏幕状态
// 查询需要的屏幕状态
- (UIInterfaceOrientationMask)supportedInterfaceOrientationsType{
    if(_isRotation) {
        //NSLog(@"🍀支持屏幕 Landscape🍀");
        if (![_rotationStatus isEqualToString:@"Landscape"]) {
            self.rotationStatus = @"Landscape";
        }
        return UIInterfaceOrientationMaskLandscape;
    }
    //NSLog(@"🍀支持屏幕 Portrait🍀");
    if (![_rotationStatus isEqualToString:@"Portrait"]) {
        self.rotationStatus = @"Portrait";
    }
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

/**
 获取当前控制器
 
 @return 当前控制器
 */
+ (UIViewController *)presentController {
    /**
     获取当前 keyWindow
     */
    UIViewController* vc = [RotationManager keyWindow].rootViewController;
    
    if (!vc) {
        vc = [UIApplication sharedApplication].windows.firstObject.rootViewController;;
    }
    while (1) {
        if ([vc isKindOfClass:[UITabBarController class]]) {
            vc = ((UITabBarController*)vc).selectedViewController;
        }
        if ([vc isKindOfClass:[UINavigationController class]]) {
            vc = ((UINavigationController*)vc).visibleViewController;
        }
        if (vc.presentedViewController) {
            vc = vc.presentedViewController;
        }else{
            break;
        }
    }
    return vc;
}


/**
 获取当前 keyWindow
 
 @return KeyWindow
 */
+ (UIWindow *)keyWindow {
    if (@available(iOS 13, *)) {
        __block UIScene * _Nonnull tmpSc;
        [[[UIApplication sharedApplication] connectedScenes] enumerateObjectsUsingBlock:^(UIScene * _Nonnull obj, BOOL * _Nonnull stop) {
            if (obj.activationState == UISceneActivationStateForegroundActive) {
                tmpSc = obj;
                *stop = YES;
            }
        }];
        
        UIWindowScene *curWinSc = (UIWindowScene *)tmpSc;
        if (!curWinSc) {
            return UIApplication.sharedApplication.delegate.window;
        }
        if (@available(iOS 15, *)) {
            return curWinSc.keyWindow;
        }else {
            UIWindow *foundWindow = curWinSc.windows.firstObject;
            for (UIWindow *window in curWinSc.windows) {
                if (window.isKeyWindow) {
                    foundWindow = window;
                    break;
                }
            }
            return foundWindow;
        }
        
    } else {
        return [UIApplication sharedApplication].keyWindow;
    }
}


///**
// 强制竖屏
// */
//- (void)forcedPortraitScreen {
//    // 在调用下面的方法前执行以下方法 configSupportedInterfaceOrientations
//    if (@available(iOS 16.0, *)) {
//        @try {
//            // [UIViewController attemptRotationToDeviceOrientation];
//            // [[RotationManager presentController].navigationController setNeedsUpdateOfSupportedInterfaceOrientations];
//            
//            UIViewController *vc = [RotationManager presentController];
//           // [vc setNeedsUpdateOfSupportedInterfaceOrientations];
//            
//            
//            [vc.navigationController setNeedsUpdateOfSupportedInterfaceOrientations];
//            
////            [vc setNeedsUpdateOfSupportedInterfaceOrientations];
//            
//            
//            
//            NSArray *array = [[[UIApplication sharedApplication] connectedScenes] allObjects];
//            UIWindowScene *scene = [array firstObject];
//           // UIInterfaceOrientationMask orientationMask = scene.interfaceOrientation == UIInterfaceOrientationPortrait ? UIInterfaceOrientationMaskLandscapeLeft : UIInterfaceOrientationMaskPortrait;
//            
//            UIInterfaceOrientationMask orientationMask = UIInterfaceOrientationMaskPortrait;
//            UIWindowSceneGeometryPreferencesIOS *geometryPreferencesIOS = [[UIWindowSceneGeometryPreferencesIOS alloc] initWithInterfaceOrientations:orientationMask];
//            [scene requestGeometryUpdateWithPreferences:geometryPreferencesIOS errorHandler:^(NSError * _Nonnull error) {
//                NSLog(@"调用了Block%@", error);
//            }];
//            
//            
////
////      [vc.navigationController setNeedsUpdateOfSupportedInterfaceOrientations];
////
////            NSArray *array = [[[UIApplication sharedApplication] connectedScenes] allObjects];
////            UIWindowScene *ws = (UIWindowScene *)[array firstObject];
////
////            Class GeometryPreferences = NSClassFromString(@"UIWindowSceneGeometryPreferencesIOS");
////            id geometryPreferences = [[GeometryPreferences alloc]init];
////            [geometryPreferences setValue:@(UIInterfaceOrientationMaskPortrait) forKey:@"interfaceOrientations"];
////            SEL sel_method = NSSelectorFromString(@"requestGeometryUpdateWithPreferences:errorHandler:");
////            void (^ErrorBlock)(NSError *err) = ^(NSError *err){
////                NSLog(@"调用了Block%@",err);
////            };
////            if ([ws respondsToSelector:sel_method]) {
////                (((void (*)(id, SEL,id,id))[ws methodForSelector:sel_method])(ws, sel_method,geometryPreferences,ErrorBlock));
////            }
////            //// 屏幕方向
////            //UIWindowSceneGeometryPreferencesIOS *geometryPreferences = [[UIWindowSceneGeometryPreferencesIOS alloc] initWithInterfaceOrientations:UIInterfaceOrientationMaskPortrait];
////            //// 开始切换
////            //[ws requestGeometryUpdateWithPreferences:geometryPreferences errorHandler:^(NSError * _Nonnull error) {
////            //    //业务代码
////            //    NSLog(@"调用了Block：%@", error);
////            //}];
//        } @catch (NSException *exception) {
//#ifdef DEBUG
//            NSLog(@"*****************************华丽分割线**************************");
//            NSLog(@"%@",exception);
//#endif
//        } @finally {
//            
//        }
//    } else {
//        // 强制旋转回来
//        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
//            SEL selector = NSSelectorFromString(@"setOrientation:");
//            NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
//            [invocation setSelector:selector];
//            [invocation setTarget:[UIDevice currentDevice]];
//            int val = UIInterfaceOrientationPortrait;
//            [invocation setArgument:&val atIndex:2];
//            [invocation invoke];
//        }
//    }
//    [UIViewController attemptRotationToDeviceOrientation];
//    [[UIDevice currentDevice] setValue:@(UIDeviceOrientationPortrait) forKey:@"orientation"];
//}



#pragma mark - 以前的横屏方法
///// 强制屏幕方向
//- (void)forcefaceOrientation {
////    if (@available(iOS 16.0, *)) {
////        [self interfaceiOS16Orientation:UIInterfaceOrientationMaskPortrait];
////    }else{
////        [self interfaceOrientation:UIInterfaceOrientationPortrait];
////    }
//}
//
//
///// iOS 16前强制屏幕方向
///// - Parameter orientation :  UIInterfaceOrientationPortrait ，UIInterfaceOrientationLandscapeRightt，UIInterfaceOrientationLandscapeLeft
//- (void)interfaceOrientation:(UIInterfaceOrientation)orientation{
//    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
//        @autoreleasepool {
//            SEL selector  = NSSelectorFromString(@"setOrientation:");
//            NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
//            [invocation setSelector:selector];
//            [invocation setTarget:[UIDevice currentDevice]];
//            NSInteger val = orientation;
//            // 从2开始是因为0 1 两个参数已经被selector和target占用
//            [invocation setArgument:&val atIndex:2];
//            [invocation invoke];
//        }
//    }
//}
//
//
///// iOS 16之后强制屏幕方向
///// - Parameter orientation :  UIInterfaceOrientationMaskPortrait ，UIInterfaceOrientationMaskLandscapeLeft ，UIInterfaceOrientationMaskLandscapeRight
//- (void)interfaceiOS16Orientation:(UIInterfaceOrientationMask)orientation {
//    if (@available(iOS 16.0, *)) {
//        [UIViewController attemptRotationToDeviceOrientation];
//
//        [self.navigationController setNeedsUpdateOfSupportedInterfaceOrientations];
//
//        NSArray *array = [[[UIApplication sharedApplication] connectedScenes] allObjects];
//
//        UIWindowScene *ws = (UIWindowScene *)array.firstObject;
//        UIWindowSceneGeometryPreferencesIOS *geometryPreferences = [[UIWindowSceneGeometryPreferencesIOS alloc] init];
//        geometryPreferences.interfaceOrientations = orientation;
//
//        [ws requestGeometryUpdateWithPreferences:geometryPreferences errorHandler:^(NSError * _Nonnull error) {
//            NSLog(@"iOS 16 转屏Error: %@",error);
//        }];
//
//    }
//}

@end
