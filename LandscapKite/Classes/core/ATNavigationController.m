//
//  ATNavigationController.m
//  EngineeringCool
//
//  Created by Mars on 2022/3/11.
//  Copyright © 2022 Mars. All rights reserved.
//

#import "ATNavigationController.h"

@interface ATNavigationController ()

@end

@implementation ATNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/**
 一般我们如此设置手势侧滑(1.先建一个NAV的子类.然后重写Push方法)
 因为count = 0的话没有上一级.这时候如果还允许侧滑就崩溃.然而如果我们自定义了导航栏的leftbarbutonitem.那么侧滑手势还是失效.

 这是因为我们的viewController.navigationController.interactivePopGestureRecognizer.delegate被重置了
 
 */
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (self.viewControllers.count == 1) {
        viewController.hidesBottomBarWhenPushed = YES;
        [super pushViewController:viewController animated:YES];
        // 让系统的侧滑返回生效
        viewController.navigationController.interactivePopGestureRecognizer.enabled = YES;
        // 支持侧滑
        viewController.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }else{
        [super pushViewController:viewController animated:YES];
    }
}

#pragma mark - 屏幕旋转
#pragma mark - - orientation


// 是否支持自动转屏
- (BOOL)shouldAutorotate {
    return [self.topViewController shouldAutorotate];
}
// 支持哪些屏幕方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.topViewController supportedInterfaceOrientations];
}
// 默认的屏幕方向（当前ViewController必须是通过模态出来的UIViewController（模态带导航的无效）方式展现出来的，才会调用这个方法）
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return [self.topViewController preferredInterfaceOrientationForPresentation];
}




@end
