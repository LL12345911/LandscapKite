//
//  RotationManager.h
//  EngineeringCool
//
//  Created by Mars on 2023/9/4.
//  Copyright © 2023 Mars. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
/**
 
 重点说明：
 1.离开横屏时、横屏中跳转，记得强制旋转至竖屏；
 2.如果没有及时旋转至横屏，会导致[UIScreen mainScreen].bounds没有及时更新，从而影响其它模块的布局问题；
 
 push 方式
 @code
 - (void)viewWillDisappear:(BOOL)animated{
     [super viewWillDisappear:animated];
     //    [RotationManager shareInstance].isRotation = NO;
 }

 - (void)viewDidLoad {
     [super viewDidLoad];
     self.view.backgroundColor = [UIColor redColor];
     [RotationManager shareInstance].isRotation = YES;
 }

 #pragma mark -
 #pragma mark - 强制横屏
 - (BOOL)shouldAutorotate {
     return YES;
 }
 
 //默认的屏幕方向
 -(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
     return  UIInterfaceOrientationLandscapeRight;
 }
 
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations {
 if (self.presentingViewController) {
     return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeRight;
 } else {
     return UIInterfaceOrientationMaskLandscapeRight;
 }
 }
 
 
 
 @code
 model Presentation 模态跳转
 
 - (void)viewWillDisappear:(BOOL)animated{
     [super viewWillDisappear:animated];
     [RotationManager shareInstance].isRotation = NO;
 }

 - (void)viewDidLoad {
     [super viewDidLoad];
    
     UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 100, 80, 50)];
     //button.center = self.view.center;
     [button setTitle:@"back" forState:UIControlStateNormal];
     [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
     [button addTarget:self action:@selector(testClick:) forControlEvents:UIControlEventTouchUpInside];
     [self.view addSubview:button];
     
     self.view.backgroundColor = [UIColor yellowColor];
     [RotationManager shareInstance].isRotation = YES;
 }

 - (void)testClick:(UIButton *)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
 }

 #pragma mark -
 #pragma mark - 强制转屏
 - (BOOL)shouldAutorotate {
     return NO;
 }

 //默认的屏幕方向
 -(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
     return  UIInterfaceOrientationLandscapeRight;
 }

 - (UIInterfaceOrientationMask)supportedInterfaceOrientations {
     return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeRight;
 }
 
 */
@interface RotationManager : NSObject

/**
 @warning Only the designated initializer should be used to create
 an instance of `shareInstance`.
 */
- (instancetype)init NS_UNAVAILABLE;

// 单例类
+ (instancetype)shareInstance;

/**
 是否横屏  YES横屏  No竖屏
 */
@property(nonatomic,assign) BOOL isRotation;


+ (void)portrait:(UIViewController *)vc;
+ (void)landscape:(UIViewController *)vc;


// 当前屏幕状态
- (UIInterfaceOrientationMask)supportedInterfaceOrientationsType;



@end

NS_ASSUME_NONNULL_END
