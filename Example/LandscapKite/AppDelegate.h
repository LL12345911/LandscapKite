//
//  AppDelegate.h
//  LandscapKite
//
//  Created by Mars on 05/21/2024.
//  Copyright (c) 2024 Mars. All rights reserved.
//

@import UIKit;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
