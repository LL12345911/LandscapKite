//
//  PrensentViewController.m
//  LandscapKite_Example
//
//  Created by Mars on 2024/5/21.
//  Copyright © 2024 Mars. All rights reserved.
//

#import "PrensentViewController.h"
#import "RotationManager.h"

@interface PrensentViewController ()

@end

@implementation PrensentViewController

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [RotationManager shareInstance].isRotation = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 100, 80, 50)];
    //button.center = self.view.center;
    [button setTitle:@"back" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(testClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    self.view.backgroundColor = [UIColor yellowColor];
    [RotationManager shareInstance].isRotation = YES;
}



- (void)testClick:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark - 强制转屏
- (BOOL)shouldAutorotate {
    return NO;
}

//默认的屏幕方向
-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return  UIInterfaceOrientationLandscapeRight;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeRight;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
