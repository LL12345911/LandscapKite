//
//  main.m
//  LandscapKite
//
//  Created by Mars on 05/21/2024.
//  Copyright (c) 2024 Mars. All rights reserved.
//

@import UIKit;
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
