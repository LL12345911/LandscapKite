//
//  ViewController.m
//  ATOrientationTool
//
//  Created by Mars on 2024/4/26.
//

#import "ViewController.h"
#import "PrensentViewController.h"
#import "TempViewController.h"



@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    self.navigationItem.title = @"测试";
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 120, 80, 50)];
    //button.center = self.view.center;
    [button setTitle:@"push" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(testClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    UIButton *button2 = [[UIButton alloc] initWithFrame:CGRectMake(140, 120, 80, 50)];
    //button.center = self.view.center;
    [button2 setTitle:@"model" forState:UIControlStateNormal];
    [button2 setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [button2 addTarget:self action:@selector(testClick2:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button2];
    
}


- (void)testClick:(UIButton *)sender {
    TempViewController *webVC = [[TempViewController alloc] init];
    [self.navigationController pushViewController:webVC animated:YES];
}


-(void)testClick2:(UIButton *)sender {
    PrensentViewController *webVC = [[PrensentViewController alloc] init];
    webVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:webVC animated:YES completion:nil];
}



- (BOOL)shouldAutorotate {
    return NO;
}

//默认的屏幕方向
-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return  UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}





@end
