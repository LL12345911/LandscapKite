//
//  TempViewController.h
//  LandscapKite_Example
//
//  Created by Mars on 2024/5/21.
//  Copyright © 2024 Mars. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TempViewController : UIViewController

/**
 是否横屏  YES横屏  No竖屏
 */
@property(nonatomic,assign) BOOL isRotation;


@end

NS_ASSUME_NONNULL_END
