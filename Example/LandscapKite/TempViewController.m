//
//  TempViewController.m
//  LandscapKite_Example
//
//  Created by Mars on 2024/5/21.
//  Copyright © 2024 Mars. All rights reserved.
//

#import "TempViewController.h"
#import "RotationManager.h"
#import "ATNavigationController.h"

@interface TempViewController ()

@end

@implementation TempViewController

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    /**
     重点说明：
       1.离开横屏时、横屏中跳转，记得强制旋转至竖屏；
       2.如果没有及时旋转至横屏，会导致[UIScreen mainScreen].bounds没有及时更新，从而影响其它模块的布局问题；
     */
    //    [RotationManager shareInstance].isRotation = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor redColor];

    [RotationManager shareInstance].isRotation = YES;
}

#pragma mark - 
#pragma mark - 强制横屏
- (BOOL)shouldAutorotate {
    return YES;
}

//默认的屏幕方向
-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return  UIInterfaceOrientationLandscapeRight;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeRight;
}

@end
