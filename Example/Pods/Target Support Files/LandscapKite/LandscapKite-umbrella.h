#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "ATNavigationController.h"
#import "ATTabBarViewController.h"
#import "RotationManager.h"
#import "UIWindow+CurrentViewController.h"
#import "LandscapKite.h"

FOUNDATION_EXPORT double LandscapKiteVersionNumber;
FOUNDATION_EXPORT const unsigned char LandscapKiteVersionString[];

